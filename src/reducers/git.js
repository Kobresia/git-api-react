const initialState = {
  users: "",
  pages: 0,
  curpage: 1
};
export default function git(state = initialState, action) {
  switch (action.type) {
    case "getUsers":
      return {
        ...state,
        users: action.users,
        pages: action.pages,
        curpage: action.curpage
      };

    default:
      return state;
  }
}
