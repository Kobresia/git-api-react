import { combineReducers } from "redux";

import git from "./git";

export default combineReducers({
  git
});
