import axios from "axios";

export function searcher(page = 1) {
  var doc = document;
  var userlist = doc.getElementById("userList");
  var preloader = doc.getElementById("preloader");
  var notify = doc.getElementById("notify");
  if (page) {
    if (doc.getElementById("userList")) userlist.style.display = "none";
    preloader.style.display = "flex";
    var name = doc.getElementById("search").value;
    return dispatch => {
      axios
        .get(
          "https://api.github.com/search/users?q=" +
            name +
            "&per_page=10&page=" +
            page
        )
        .then(function(data) {
          var items = data.data.items;
          if (items.length > 0) {
            dispatch({
              type: "getUsers",
              users: items,
              curpage: parseInt(page,0),
              pages: Math.ceil(data.data.total_count / 10)
            });
            notify.style.display = "none";
            userlist.style.display = "block";
          } else {
            notify.style.display = "block";
            userlist.style.display = "none";
            notify.innerHTML = "There are no users with entered username";
          }
          preloader.style.display = "none";
        })
        .catch(function(error) {
          preloader.style.display = "none";
          userlist.style.display = "none";
          notify.style.display = "block";
          notify.innerHTML =
            "API rate limit exceeded.Rate limit may take several minutes to update.";
        });
    };
  }
}
