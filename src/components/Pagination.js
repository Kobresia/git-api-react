import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class Pagination extends Component {
  render() {
    const {git, searcher} = this.props;
    var dis=git.curpage + 1 > git.pages? "disabled" : "";
    var dis1=git.curpage - 1 <= 0? "disabled":"";
                  

  	return(
              <div className="pagination">
                <button
                  type="button"
                  className="waves-effect waves-light btn"
                  disabled={dis1}
                  onClick={this.props.searcher.bind(
                    this,
                    git.curpage - 1 <= 0 ? false : git.curpage - 1
                  )}
                >
                  <i className="material-icons left">chevron_left</i>{" "}
                </button>
                <div>
                  Pages {git.curpage} / {git.pages}
                </div>
                <button
                  type="button"
                  className="waves-effect waves-light btn"
                  disabled={dis}
                  onClick={searcher.bind(
                    this,
                    git.curpage + 1 > git.pages ? false : git.curpage + 1
                  )}
                >
                  {" "}
                  <i className="material-icons left">chevron_right</i>{" "}
                </button>
            </div>

  		);

  }

}
Pagination.propTypes = {

  git:PropTypes.shape({
    curpage: PropTypes.number.isRequired,
    pages: PropTypes.number.isRequired
  }).isRequired,
  searcher: PropTypes.func.isRequired,

}

