import React, { Component } from "react";
import Preloader from "./Preloader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "./../actions/actions";

import Pagination from "./Pagination";
  class Git extends Component {
  render() {

    const { searcher } = this.props.actions;
    const { git } = this.props;
 

    return (
      <div className="gitForm">
        <div className="formSearch">
          <input
            id="search"
            name="search"
            type="text"
            placeholder="Browse users via the GitHub API"
          />
          <button
            type="button"
            className="waves-effect waves-light btn"
            ref="gobtn"
            onClick={searcher.bind(this, 1)}
          >
            Search <i className="material-icons">search</i>{" "}
          </button>
        </div>
        <Preloader/>
        <div className="card-panel teal lighten-3" id="notify" />

        <div className="userList" id="userList">
          {git.users ? (
            <div>
              {" "}
              <ul className="collection" id="collection">
                {git.users.map(function(m, index) {

                  return (
                    <li className="collection-item avatar" key={index}>
                      <a href={m.html_url} target="_blank">
                        <img src={m.avatar_url} alt="" className="circle" />
                        <span className="title">{m.login}</span>
                      </a>
                    </li>
                  );
                })}
              </ul>
              <Pagination searcher={searcher} git={git} />
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    git: state.git
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Git);
